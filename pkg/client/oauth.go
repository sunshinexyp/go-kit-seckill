package client


import (
	"context"
	"github.com/longjoy/micro-go-book/ch13-seckill/pb"
	"github.com/longjoy/micro-go-book/ch13-seckill/pkg/discover"
	"github.com/longjoy/micro-go-book/ch13-seckill/pkg/loadbalance"
	"github.com/opentracing/opentracing-go"
)

type OAuthClient interface {
	//校验用户token
	CheckToken(ctx context.Context,tracer opentracing.Tracer, request *pb.CheckTokenRequest)(*pb.CheckTokenResponse,error)
}

type OAuthClientImpl struct {
	//客户端管理器
	manager ClientManager
	//服务名称
	serverName string
	//负载均衡策略
	loadBalance loadbalance.LoadBalance
	//链路追踪系统
	tracer opentracing_go.Tracer
}

func (impl *OAuthClientImpl) CheckToken(ctx context.Context,tracer opentracing.Tracer, request *pb.CheckTokenRequest)(*pb.CheckTokenResponse,error) {
reponse := new(pb.CheckTokenResponse)
if err := impl.manager.DecoratorInvoke("/pb.OAuthService/CheckToken","token_check",tracer,ctx,request,reponse);
	err ==  nil {
		return reponse,nil
	}else{
		return nil,err
}
}

//生成OAuthClient的工厂方法，它会初始化OAuthClientImpl实例
func NewOAuthClient(serviceName string, lb loadbalance.LoadBalance, tracer opentracing.Tracer) (OAuthClient, error) {
	if serviceName == "" {
		serviceName = "oauth"
	}
	if lb == nil {
		lb = defaultLoadBalance
	}

	return &OAuthClientImpl{
		manager: &DefaultClientManager{
			serviceName: serviceName,
			loadBalance: lb,
			discoveryClient:discover.ConsulService,
			logger:discover.Logger,
		},
		serviceName: serviceName,
		loadBalance: lb,
		tracer:      tracer,
	}, nil

}


