package discover

import (
	"github.com/hashicorp/consul/api"
	"github.com/longjoy/micro-go-book/ch13-seckill/pkg/common"
	"github.com/go-kit/kit/sd/consul"
	"log"
	"sync"
)

//定义discovery_client接口
type DiscoveryClient interface {
	/**
		服务注册接口
		serviceName 服务名
		instanceId  服务实例ID
		instancePort 服务实施端口
		healthCheckUrl 健康检查地址
		weight 权重
		meta   服务实例元数据
	 */
	Register(instanceId, svcHost,healthCheckUrl,svcPort string,svcName string,weight int, meta map[string]string,tags []string,
		logger *log.Logger)bool

	/**
		服务注销接口
	 */
	DeRegister(instanceId string,logger *log.Logger) bool

	/**
		发现服务实例接口
		serviceName 服务名
	 */
	DiscoverServices(serviceName string,logger *log.Logger) []*common.ServiceInstance
}

type DiscoveryClientInstance struct {
	Host string
	Port int
	//连接consul的配置
	config *api.Config
	client consul.Client
	mutex sync.Mutex
	//服务实例缓存字段
	instancesMap sync.Map
}