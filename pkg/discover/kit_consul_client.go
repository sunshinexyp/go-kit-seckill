package discover

import (
	"github.com/hashicorp/consul/api"
	"github.com/hashicorp/consul/api/watch"
	"github.com/longjoy/micro-go-book/ch13-seckill/pkg/common"
	"github.com/go-kit/kit/sd/consul"
	"log"
	"strconv"
	"fmt"
)

func (consulClient *discover.DiscoveryClientInstance)Register(instanceId ,svcHost,healthCheckUrl,svcPort string ,svcName string , weight int ,
	meta map[string]string,tags []string ,logger *log.Logger) bool{
	//字符串转成整形
	port,_ := strconv.Atoi(svcPort)

	//1、构建服务实例元数据
	fmt.Println(weight)
	serviceRegistration := &api.AgentServiceRegistration{
		ID: instanceId,
		Name : svcName,
		Address:svcHost,
		Port: port,
		Meta : meta,
		Tags : tags,
		Weights : &api.AgentWeights{
			Passing:weight,
		},
		Check:&api.AgentServiceCheck{
			DeregisterCriticalServiceAfter : "30s",
			HTTP:"http://" + svcHost + ":" + strconv.Itoa(port) + healthCheckUrl,
			Interval : "15s",
		},

	}
	//2、发送服务注册到consul中
	err := consulClient.client.Register(serviceRegistration)
	if err != nil {
		logger.Println("Register Service Error!")
		return false
	}
	logger.Println("Register Service Success")
	return true

}

func (consulClient *discover.DiscoveryClientInstance) DeRegister(instanceId string,logger *log.Logger) bool {
	serviceRegistration := &api.AgentServiceRegistration{
		ID:instanceId,
	}
	err := consulClient.client.Deregister(serviceRegistration)
	if err != nil {
		logger.Println("DeRegister Service Error!")
		return false
	}
	logger.Println("DeRegister Service Success")
	return true
}
/**
	根据服务名称获取服务注册和发现中心的服务实例列表
	将从consul中获取的服务实例列表缓存在客户端本地DiscoveryClientInstance的intanceMap中
	并注册对该服务状态的监控，之后再调用该方法来获取服务实例时，则直接从本地缓存中获取，
	当有新的服务实例上线或者旧实例下线时，对服务实例监控就可以实时看到，并更新本地缓存的服务实例表
 */
func (consulClient *discover.DiscoveryClientInstance) DiscoverServices(serviceName string,logger *log.Logger) []*common.ServiceInstance{
	//该服务已监控并缓存
	instanceList,ok := consulClient.instancesMap.Load(serviceName)
	if ok {
		return instanceList.([]*common.ServiceInstance) //这里是啥语法？？？？？
	}
	//申请锁
	consulClient.mutex.Lock()
	//再次检查是否监控
	instanceList ,ok := consulClient.instanceMap.Load(serviceName)
	if ok {
		return instanceList.([]*common.ServiceInstance) //这里是啥语法？？？？？
	} else {
		//注册监控
		go func(){
			params := make(map[string]interface{}) //interface{}表示可以存任何类型
			params["type"] = "service"
			params["service"] = serviceName
			plan,_ :=watch.Parse(params)
			plan.Handler = func(u uint64, i interface{}) {
				if i==nil {
					return
				}
				v,ok := i.([]*api.ServiceEntry)
				if !ok {
					return
				}

				//没有服务实例在线
				if len(v) == 0 {
					consulClient.instanceMap.Store(serviceName,[]*common.ServiceInstance{})
				}
				var healthServices []*common.ServiceInstance

				for _,service :=range v{
					if service.Checks.AggregatedStatus() == api.HealthPassing {
						healthServices = append(healthServices,newServiceInstance(service.Service))
					}
				}
				//缓存该服务名下的服务实例
				consulClient.instancesMap.Store(serviceName, healthServices)

			}
			defer plan.Stop()
			plan.Run(consulClient.config.Address)

		}()
	}
	defer  consulClient.mutex.Unlock()
	//根据服务名请求服务实例列表
	entries ,_,err := consulClient.client.Service(serviceName,"",false,nil)
	if err != nil {
		consulClient.instanceMap.Store(serviceName,[]*common.ServiceInstance{})
		if logger != nil {
			logger.Println("Discover Service Error")
		}
		return nil
	}
	instances :=make([]*common.ServiceInstance,len(entries))
	for i:=0;i<len(instances);i++ {
		instances[i] = newServiceInstance(entries[i].Service)
	}
	consulClient.instanceMap.Store(serviceName,instances)
	return instances
}