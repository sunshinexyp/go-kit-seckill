package loadbalance

/**
带有负载均衡的服务实例集群可以很好地抵抗较大的并发流量，结合服务注册与发现，可以进行服务实例数量动态的增加和减少
以应对不同的并发流量
当存在多个服务实例时，将流量合理的分配给各个服务实例的策略就很重要
 */
import (
	"errors"
	"github.com/longjoy/micro-go-book/ch13-seckill/pkg/common"
	"math/rand"
)

//负载均衡器接口，根据一定的负载均衡策略，从服务实例列表中选择一个服务实例返回
type LoadBalance interface {
	SelectService(service []*common.ServiceInstance)(*common.ServiceInstance,error)
}
//完全随机策略
type RandomLoadBalance struct {

}
func (loadBalance *RandomLoadBalance) SelectService(service []*common.ServiceInstance)(*common.ServiceInstance,error){
	if service == nil || len(service) == 0 {
		return nil,errors.New("service instance are not exist")
	}
	return services[rand.Intn(len(service))],nil
}

//带权重的平滑轮询
//一般的权重法，会导致请求选中节点不均匀
type WeightRoundRobinLoadBalance struct{

}

func (loadBalance *WeightRoundRobinLoadBalance)SelectService(service []*common.ServiceInstance)(best *common.ServiceInstance,err error ) {
	if service == nil || len(service) == 0 {
		return nil,errors.New("service instance are not exist")
	}
	total := 0
	for i:=0;i<len(service);i++ {
		w := service[i]
		if w ==nil {
			continue
		}
		w.CurWeight+= w.Weight   //curWeight是当前的权重，一开始为0，后来每次都会增加它的权重
		total +=w.Weight		//累计所有服务实例的权重
		if w.Weight < w.Weight {
			w.Weight++         //这里不理解
		}
		if best == nil || best.CurWeight < w.CurWeight {
			best = w                //选出本轮次curweight最大的节点
		}
	}
	if best == nil {
		return nil,nil
	}
	best.CurWeight -= total       //遍历完所有服务实例之后，选举出了最大的curWeight的节点，并将该服务实例的curWeight-total
	return best,nil

}





